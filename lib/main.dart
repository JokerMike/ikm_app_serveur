import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:ikm_server/home.dart';
import 'package:ikm_server/utilities/sharedPreferences.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'IK Money server',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }
}

class _MyAppState extends State<MyApp>{

  bool inittialized = false;

  @override
  void initState() {
    super.initState();
    SharedPreferencesClass.restore("initialized").then((onValue){
      setState(() {
        inittialized = onValue;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'IK Money server',
      theme: ThemeData(
          primaryColor: Colors.blue,
      ),
      home: inittialized?Home():MyHomePage(title: 'IK Money serveur'),
    );
  }

}








class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _phoneNumberController = TextEditingController();
  TextEditingController _codeController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(20.0),
                child: Text("Saisissez le numéro admin du réseau que le téléphone gerera"),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: TextFormField(
                  decoration: InputDecoration(
                    hintText: "Numéro admin",
                    helperText: "Numéro précédé de l'indicatif, Ex : +228"
                  ),
                  controller: _phoneNumberController,
                  validator: ((String value) {
                    if (value.isEmpty) {
                      return "Précisez le numéro de téléphone";
                    }
                    if (value.length < 8) {
                      return "Le numéro de téléphone fais 8 caratères au moins";
                    }
                  }),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
                child: TextFormField(
                  decoration: InputDecoration(
                      hintText: "Code envoi",
                      helperText: "Code ussd d'envoi d'argent"
                  ),
                  controller: _codeController,

                  validator: ((String value){
                    if (value.isEmpty) {
                      return "Précisez le code de transfert";
                    }
                    if (value.length < 2) {
                      return "Trop court";
                    }
                  }),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0,right: 20.0,left: 20.0),
                child: RaisedButton(
                  child: Text("Valider"),
                  onPressed: (){
                    if(_formKey.currentState.validate()){
                      SharedPreferencesClass.save("initialized", true);
                      SharedPreferencesClass.save("phoneNumber", _phoneNumberController.text);
                      SharedPreferencesClass.save("code", _codeController.text);
                      Navigator.pushReplacement(context,MaterialPageRoute(
                          builder: (context) =>Home()
                      ));
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
