import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:ikm_server/utilities/sharedPreferences.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sim_service/models/sim_data.dart';
import 'package:sim_service/sim_service.dart';
import 'package:ussd/ussd.dart';
import 'package:ussd_service/ussd_service.dart';
import 'package:flutter/services.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

enum RequestState {
  Ongoing,
  Success,
  Error,
}

class _HomeState extends State<Home> {
  Socket socket;
  String phoneNumber = "";
  String code = "";

  RequestState _requestState;
  String _requestCode = "*155#";
  String _responseCode = "";
  String _responseMessage = "";

 static const methodChannel = const MethodChannel('ussdcode');
  void dataHandler(data) {
    String s = String.fromCharCodes(data).trim();
    var data_array = s.split(":");

    print(new String.fromCharCodes(data).trim());
    if (s.substring(0, 6) == "TINFO:") {
      debugPrint("Here");
      // Exécuter le code ussd
      var c = code.replaceAll("[_montant_]", data_array[3]);
      c = c.replaceAll("[_num_]", data_array[2]);
      debugPrint("Here::" + c);
      int cpt = 0;
      do {
        makeMyRequest(c);
        cpt++;
      } while ((cpt < 3) && _requestState == RequestState.Error);
      socket.write("Transaction initiée");
    }
  }

  void errorHandler(error, StackTrace trace) {
    print(error);
  }

  void doneHandler() {
    socket.destroy();
  }

  @override
  void initState() {
    super.initState();
    SharedPreferencesClass.restore("phoneNumber").then((value) {
      setState(() {
        phoneNumber = value;
      });
    });
    SharedPreferencesClass.restore("code").then((value) {
      setState(() {
        code = value;
      });
      Socket.connect("192.168.1.196", 8585).then((Socket sock) {
        socket = sock;
        socket.write("NUM:" + phoneNumber);
        socket.listen(dataHandler,
            onError: errorHandler, onDone: doneHandler, cancelOnError: false);
      }).catchError((Object e) {
        print("Unable to connect: $e");
      });
      //Connect standard in to the socket
      //stdin.listen((data) => socket.write(new String.fromCharCodes(data).trim() + '\n'));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("IK Money Server"),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Icon(Icons.desktop_windows),
                ),
                Expanded(
                  flex: 5,
                  child: Text("Le serveur a démarré"),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 20.0, right: 20.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Icon(Icons.phone_android),
                ),
                Expanded(
                  flex: 5,
                  child: Text("Numéro d'écoute : " + phoneNumber.toString()),
                ),
              ],
            ),
          ),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Future<void> sendUssdRequest(String code) async {
    setState(() {
      _requestState = RequestState.Ongoing;
    });
    try {
      String responseMessage;
      await PermissionHandler().requestPermissions([PermissionGroup.phone]);
      SimData simData = await SimService.getSimData;
      if (simData == null) {
        debugPrint("simData cant be null");
        return;
      }
      responseMessage = await UssdService.makeRequest(
          simData.cards.first.subscriptionId, _requestCode);
      setState(() {
        _requestState = RequestState.Success;
        _responseMessage = responseMessage;
      });
    } catch (e) {
      setState(() {
        _requestState = RequestState.Error;
        _responseCode = e.code;
        _responseMessage = e.message;
      });
    }
  }
  

  Future<String> _sendUssdcode(int subsubscriptionId, String code) async {
    String _message;
    try {
      dynamic result = await methodChannel.invokeMethod(
          'codeUssd', {'subscriptionId':subsubscriptionId , 'code': code});
      _message = result;
    } on PlatformException catch (e) {
      _message = "Can't do native stuff ${e.message}.";
    }
    return _message;
  }

  Future makeMyRequest(String code) async {
     String ussdResponseMessage ;
    SimData simData = await SimService.getSimData;
    int subscriptionId =simData.cards.first.subscriptionId; // sim card subscription ID
    setState(() {
      _requestState = RequestState.Ongoing;
    });
    try {

       _sendUssdcode(subscriptionId, code).then((value){
           sleep(const Duration(seconds:2));
              setState(() {
                  _requestState = RequestState.Success;
                  ussdResponseMessage=value;
                });
       });
    
      print("succes! message: $ussdResponseMessage");
      
    } catch (e) {
      debugPrint("error! code: ${e.code} - message: ${e.message}");
      setState(() {
        _requestState = RequestState.Error;
      });
    }
  }
}
