package com.example.ikm;
import android.util.Log;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.plugin.common.MethodChannel;
import ussdcontroller.USSDApi;
import ussdcontroller.USSDController;

public class MainActivity extends FlutterActivity {
    private static final String CHANNEL = "ussdcode";
    private String response;
    @Override
    public void configureFlutterEngine(FlutterEngine flutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);

        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL)
        .setMethodCallHandler((call, result) -> {
            if (call.method.equals("codeUssd")) 
            {

         final USSDApi ussdApi = USSDController.getInstance(this);
                ussdApi.callUSSDInvoke(call.argument("code"),call.argument("subscriptionId"), new USSDController.CallbackInvoke()  {
                @Override
                    public void responseInvoke(String message) {
                        /*response=message;
                        Log.d("MON TRAITEMENT", message);*/
                    }

                    @Override
                    public void over(String message) {
                       response=message;
                        Log.d("MON TRAITEMENT",message);
                    }
                });
                result.success(response);
            }else{
                result.notImplemented();
            }

        });
    }
  }